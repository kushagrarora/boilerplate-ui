import React from "react";

function Card() {
  return <div className="bg-gray-100 text-gray-800 p-4 rounded-md my-6 flex flex-col md:flex-row md:items-center shadow-md">
    <figure className="mx-auto mb-3 md:mb-0 rounded-full overflow-hidden w-20 h-20 md:ml-0 md:mr-20 md:w-32 md:h-32">
      <img src="http://placekitten.com/200/200" alt="" className="max-w-full max-h-full"/>
    </figure>
    <div className="md:flex-grow">
      <h2 className="text-lg text-gray-900 text-center md:text-left mb-3 md:text-2xl">Soft Kitty</h2>
      <p className="text-gray-600 italic text-sm text-center md:text-left md:text-lg">Warm kitty, little ball of fur</p>
    </div>
  </div>;
}

export default Card;
