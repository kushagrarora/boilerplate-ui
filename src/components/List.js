import React from "react";
import PropTypes from "prop-types";
import Card from "./Card";

function List(props) {
  return (
    <div className="mb-3 mx-3">
      <Card />
      <Card />
      <Card />
    </div>
  );
}

List.propTypes = {
  list: PropTypes.array,
};

export default List;
