import React from "react";

export default function Footer() {
  return (
    <footer className="bg-purple-900 py-8 px-3 text-purple-200 text-center">
      Made with{" "}
      <span role="img" aria-label="heart">
        ❤️
      </span>{" "}
      and{" "}
      <span role="img" aria-label="coffee">
        ☕
      </span>{" "}
      by <a href="mailto:kushagrarora.17@gmail.com" className="underline">Kushagr Arora</a>
    </footer>
  );
}
