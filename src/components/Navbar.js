import React from "react";

export default function Navbar() {
  return (
    <nav className="bg-purple-800 p-4 text-gray-200 fixed shadow-md inset-x-0 top-0">
      <div className="container px-2 mx-auto max-w-screen-md">
        <div className="flex justify-between">
          <h1 className="text-xl md:text-2xl">Treebo Hotels</h1>
          <a
            href="https://gitlab.com/kushagrarora/treebo-frontend"
            target="_blank"
            rel="noopener noreferrer"
            className="inline-block text-bold border px-2 py-1 rounded shadow-xs md:leading-6">
            Source Code
          </a>
        </div>
      </div>
    </nav>
  );
}
