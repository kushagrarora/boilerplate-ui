import React from "react";
import SearchBar from "./SearchBar";
import List from "./List";

export default function Content() {
  return <div className="container px-3 max-w-screen-md mx-auto md:px-0">
    <SearchBar />
    <List />
  </div>;
}
