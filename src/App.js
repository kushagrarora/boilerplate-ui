import React from 'react';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Content from './components/Content';

function App() {
  return (
    <div className="bg-purple-300">
      <div className="pt-20">
        <Navbar />
      </div>
      <Content />
      <Footer />
    </div>
  );
}

export default App;
