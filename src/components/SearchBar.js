import React, { useState } from "react";

export default function SearchBar(props) {
  const [text, setText] = useState("");

  return (
    <form className="flex mb-5 text-sm mt-2 md:text-base">
      <input
        value={text}
        onChange={(ev) => setText(ev.target.value)}
        type="text"
        className="p-2 rounded mx-3 text-gray-700 flex-grow shadow-md"
        placeholder="Type here to search..."
      />
      <input
        type="submit"
        value="Search"
        className="px-2 mr-3 rounded shadow-xs bg-transparent flex-grow border border-purple-800 text-purple-800 hover:bg-purple-800 hover:text-gray-100 md:flex-grow-0"
      />
    </form>
  );
}
